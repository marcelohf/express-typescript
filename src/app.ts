import express from "express";
import { ApolloServer } from "apollo-server-express";

import indexRoute from "./routes/index";
import schema from "./graphql/schema";

let app = express();

app.get("/", indexRoute);

const server = new ApolloServer({
  schema,
  playground: false,
  introspection: true
});

server.applyMiddleware({
  app
});

export default app;
