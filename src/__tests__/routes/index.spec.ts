import request from "supertest";
import app from "../../app";
import packageInfo from "pjson";

describe("app", () => {
  it("should return app name and version on status page", () => {
    return request(app)
      .get("/")
      .expect(200)
      .then(response => {
        expect(response.text).toBe(
          `<h2>${packageInfo.name} - ${packageInfo.version}</h2>`
        );
      });
  });
});
