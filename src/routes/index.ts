import express from "express";
import packageInfo from "pjson";

export default (_: express.Request, res: express.Response) => {
  res.send(`<h2>${packageInfo.name} - ${packageInfo.version}</h2>`);
};
