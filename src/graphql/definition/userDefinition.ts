import { gql } from "apollo-server-express";
import User from "src/models/user";

const typeDefs = gql`
  extend type Query {
    """
    User list
    """
    users: [User]
  }

  type User {
    """
    user id
    """
    id: Float!
    """
    user login
    """
    login: String!
    """
    user name
    """
    name: String
  }
`;

const resolvers = {
  Query: {
    users: () => {
      return [
        new User(1, "bob", "Bob McDonald"),
        new User(2, "bla", "Mr. Bla"),
        new User(10, "ble", "Mrs. Ble")
      ];
    }
  }
};

export default { typeDefs, resolvers };
