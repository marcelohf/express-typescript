import { makeExecutableSchema } from "apollo-server-express";
import { merge } from "lodash";

let definitions = [];
definitions.push(require("./definition/userDefinition").default);

const Query = `
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }
`;

export default makeExecutableSchema({
  typeDefs: [Query, ...definitions.map(d => d.typeDefs)],
  resolvers: merge({}, ...definitions.map(d => d.resolvers))
});
